package com.citysmart.demo.springboot.config;

import freemarker.ext.jsp.TaglibFactory;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

/**
 * Guocg
 * 2018/11/8
 */
@Configuration
public class FreemarkerConfig {

    @Autowired
    private FreeMarkerProperties properties;

    protected void applyProperties(FreeMarkerConfigurationFactory con) {
        con.setTemplateLoaderPaths(this.properties.getTemplateLoaderPath());
        con.setPreferFileSystemAccess(this.properties.isPreferFileSystemAccess());
        con.setDefaultEncoding(this.properties.getCharsetName());
        Properties settings = new Properties();
        settings.putAll(this.properties.getSettings());
        con.setFreemarkerSettings(settings);
    }

    @Bean
    public FreeMarkerConfigurer freeMarkerConfigurer(
            ServletContext servletContext) throws IOException, TemplateException {
        FreeMarkerConfigurer con = new FreeMarkerConfigurer();
        con.setConfigLocation(new ClassPathResource("freemarker.properties"));
        applyProperties(con);

        con.setServletContext(servletContext);
        TaglibFactory factory = con.getTaglibFactory();
        if (factory.getObjectWrapper() == null) {
            factory.setObjectWrapper(con
                    .createConfiguration()
                    .getObjectWrapper());
        }
//        factory.setClasspathTlds(Arrays.asList("/tld/shiro.tld", "/tld/log.tld"));
        return con;
    }

}
