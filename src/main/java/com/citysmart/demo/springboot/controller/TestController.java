package com.citysmart.demo.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Guocg
 * 2018/11/9
 */
@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(TestController.class);
    @GetMapping("/test")
    public String sdf() {
        log.debug("MYLOG00020:就是这里");
        log.warn("sdf");
        return "sdf";
    }
}
